package Locator;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Child_browser_handle {

	public static void main(String[] args)
	{
    System.setProperty("webdriver.gecko.driver", ".\\driver\\geckodriver.exe");
    WebDriver driver= new FirefoxDriver();
	driver.get("https://www.naukri.com/");	
	String windowHandle = driver.getWindowHandle();
	System.out.println(windowHandle);
	}

}
