package Locator;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.gargoylesoftware.htmlunit.javascript.host.Set;
import com.gargoylesoftware.htmlunit.javascript.host.file.File;
import java.io.FileWriter;

public class MC_loginTestNg extends BaseClass 
{
	WebDriver driver;
    
@Test(description="valid Login",priority=1)

public void validloginapplication() throws Exception 
{
  
	driver.findElement(By.id("username")).sendKeys("admin");
	
	driver.findElement(By.id("password")).sendKeys("1");//for valid login
	
    driver.findElement(By.id("input_submit")).click();
    

	  String message1 = "Login Successfully";
	  Assert.assertEquals("Login Successfully", message1);
    
    System.out.println(" message1");
    
  //  driver.findElement(By.xpath("//div[contains(text(),'Sign up')]"));
    
    logResults("Login Successfully");
}
@Test(description="invalid Login",priority=2)

public void invalidloginapplication() 
{
driver.get("http://192.168.1.196/managementconsole/welcome");
	
driver.findElement(By.id("username")).sendKeys("admin");
driver.findElement(By.id("password")).sendKeys("2");//for invalid username and password

driver.findElement(By.id("input_submit")).click();

String message2 = "Invalid username or password";
Assert.assertEquals("Invalid username or password", message2);

System.out.println("message2");

}

@Test(description="password field is blank",priority=3)

public void BlankPassword() 
{
driver.get("http://192.168.1.196/managementconsole/welcome");
	
driver.findElement(By.id("username")).sendKeys("admin");
driver.findElement(By.id("password")).sendKeys(" ");//blank password

driver.findElement(By.id("input_submit")).click();

String message3 = "Enter password";
Assert.assertEquals("Enter password", message3);

System.out.println("message3");


}
@Test(description="username field is blank",priority=4)

public void BlankUsername() 
{
driver.get("http://192.168.1.196/managementconsole/welcome");
	
driver.findElement(By.id("username")).sendKeys(" ");//blank username
driver.findElement(By.id("password")).sendKeys("1");

driver.findElement(By.id("input_submit")).click();

String message4 = "Enter username";
Assert.assertEquals("Enter username", message4);

System.out.println("Enter username");


}
@BeforeMethod

public void beforeMethod() 
{
driver = new FirefoxDriver();
  
driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

driver.get("http://192.168.1.196/managementconsole/welcome");
    
}

@AfterMethod
public void afterMethod() {
	driver.close();
	//driver.quit();
}

//public void logResults(fileName,messageBody,statusType)
public void logResults(String message) throws Exception
{

    String csvFile = "C:\\Users\\aarti\\eclipse-workspace\\AutomationProject\\logs\\xyzabc.csv";
    FileWriter writer = new FileWriter(csvFile);
    StringBuilder sb = new StringBuilder();
    
    sb.append(message);

    writer.flush();
    writer.close();
	
//       try{
//        	Assert.assertEquals(driver.findElement(By.xpath(prop.getProperty("failedlogin"))).getText(), "LOG IN");
//          //add pass entry to the excel sheet
//            testresultdata.put("3", new Object[] {2d, "User should not be able to login with an invalid password", "Login failed","Pass"});
//          }
//
//          catch(Exception e)
//          {
//            //add fail entry to the excel sheet
//            testresultdata.put("3", new Object[] {2d, "User should not be able to login with an invalid password", "Login failed","Fail"});
//          }           
//
        
    
	
	
}
}
