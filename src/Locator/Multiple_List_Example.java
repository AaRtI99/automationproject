package Locator;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Multiple_List_Example {

	public static void main(String[] args) 
	{
		 System.setProperty("webdriver.gecko.driver", ".\\driver\\geckodriver.exe");
		    WebDriver driver= new FirefoxDriver();
			driver.get("file:///C:/Users/aarti/Documents/multipledropdown.html");
			WebElement listbox = driver.findElement(By.id("mtr"));
			Select s=new Select(listbox);
			List<WebElement> allOptions = s.getOptions();
			System.out.println(allOptions.size() +"Total count");
			System.out.println("** print all the value of listbox**");
			for (WebElement option : allOptions) 
			{
				System.out.println(option.getText());
			}
			//s.selectByIndex(0);//IDLY
			s.selectByValue("d");//DOSA
		//	s.selectByVisibleText("poori");
					

	}

}
