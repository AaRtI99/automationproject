package Locator;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


public class ActionMouseHover extends BaseClass
{

	public static void main(String[] args) throws InterruptedException 
	{
		driver.get("http://www.dhtmlgoodies.com/submitted-scripts/i-google-like-drag-drop/index.html");
		
		WebElement srcblock1= driver.findElement(By.xpath("//h1[text()='Block 1']"));
		
		
		WebElement destblock3= driver.findElement(By.xpath("//h1[text()='Block 3']"));
		
		Actions action= new Actions(driver);
		
		Thread.sleep(2000);
		
		action.dragAndDrop(srcblock1,destblock3).perform();
		
		driver.close();
		
		
		

	}

}
