package Locator;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TestMCLogin extends BaseClass {

  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://192.168.1.173/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testMCLogin() throws Exception {
    driver.get(baseUrl + "/managementconsole/welcome");
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("locusplay");
    driver.findElement(By.id("input_submit")).click();
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("locusplay");
	
    driver.findElement(By.xpath("(//a[contains(text(),'Head Location')])[2]")).click();
    driver.findElement(By.id("Location-butt")).click();
    driver.findElement(By.id("txtlocationname")).clear();
    driver.findElement(By.id("txtlocationname")).sendKeys("Bangalore");
    driver.findElement(By.id("ofc_phone_1")).clear();
    driver.findElement(By.id("ofc_phone_1")).sendKeys("080123456");
    driver.findElement(By.id("btn_location_save")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
	
    driver.findElement(By.linkText("Location")).click();
    driver.findElement(By.id("Location-butt")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[4]")).click();
    driver.findElement(By.id("ui-multiselect-inp_head_location-option-0")).click();
    driver.findElement(By.id("txtlocationname")).clear();
    driver.findElement(By.id("txtlocationname")).sendKeys("BTM Layout");
    driver.findElement(By.id("txtcreditlimit")).clear();
    driver.findElement(By.id("txtcreditlimit")).sendKeys("12000");
    driver.findElement(By.id("setting_password")).clear();
    driver.findElement(By.id("setting_password")).sendKeys("1234");
    driver.findElement(By.id("butt-Receipt-Info")).click();
    driver.findElement(By.id("butt-Add-Location")).click();
    driver.findElement(By.id("btn_location_save")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
    driver.findElement(By.linkText("User")).click();
	
    driver.findElement(By.id("btn_add_adminmanager")).click();
    driver.findElement(By.id("uname")).clear();
    driver.findElement(By.id("uname")).sendKeys("admin");
    driver.findElement(By.id("upassword")).clear();
    driver.findElement(By.id("upassword")).sendKeys("Aqui#123");
    driver.findElement(By.xpath("(//button[@type='button'])[5]")).click();
    driver.findElement(By.id("ui-multiselect-ddl_location-option-0")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[6]")).click();
    driver.findElement(By.xpath("//div[14]/ul/li/label")).click();
    driver.findElement(By.id("ui-multiselect-ddl_profile-option-0")).click();
    driver.findElement(By.id("firstname")).clear();
    driver.findElement(By.id("firstname")).sendKeys("LP");
    driver.findElement(By.id("lastname")).clear();
    driver.findElement(By.id("lastname")).sendKeys("Admin");
    driver.findElement(By.id("btn_save_adminmanager")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
    driver.findElement(By.cssSelector("a.arr1.logoutArrow")).click();
    driver.findElement(By.linkText("Sign Out")).click();
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("1");
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("admin");
    driver.findElement(By.id("input_submit")).click();
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("Aqui#123");
	
    driver.findElement(By.cssSelector("span.txt_Games")).click();
    driver.findElement(By.id("btnpick3")).click();
    driver.findElement(By.id("btnpick3gameadd")).click();
    driver.findElement(By.id("pick3rdboth")).click();
    driver.findElement(By.id("pick3earlyflimg234ball")).clear();
    driver.findElement(By.id("pick3earlyflimg234ball")).sendKeys("/home/rahul/Desktop/images/borletteEarly.jpg");
    driver.findElement(By.id("pick3lateflimg234ball")).clear();
    driver.findElement(By.id("pick3lateflimg234ball")).sendKeys("/home/rahul/Desktop/images/Albarka.png");
    driver.findElement(By.id("pick3gamename234ball")).click();
    driver.findElement(By.id("pick3gamename234ball")).click();
    driver.findElement(By.id("pick3gamename234ball")).clear();
    driver.findElement(By.id("pick3gamename234ball")).sendKeys("Chicago");
    driver.findElement(By.id("pick3gamename234ball")).click();
    driver.findElement(By.id("pick3earlygamename")).click();
    driver.findElement(By.id("pick3earlygamename")).clear();
    driver.findElement(By.id("pick3earlygamename")).sendKeys("Early cago");
    driver.findElement(By.id("pick3gamename234ball")).click();
    driver.findElement(By.id("pick3earlygamename")).click();
    driver.findElement(By.id("pick3earlygamename")).clear();
    driver.findElement(By.id("pick3earlygamename")).sendKeys("Early Chicago");
    driver.findElement(By.id("pick3earlygameprefix")).click();
    driver.findElement(By.id("pick3earlygameprefix")).click();
    driver.findElement(By.id("pick3earlygameprefix")).clear();
    driver.findElement(By.id("pick3earlygameprefix")).sendKeys("ECH");
    driver.findElement(By.id("pick3lategamename")).click();
    driver.findElement(By.id("pick3lategamename")).clear();
    driver.findElement(By.id("pick3lategamename")).sendKeys("Late Chicago");
    driver.findElement(By.id("pick3lategameprefix")).click();
    driver.findElement(By.id("pick3lategameprefix")).clear();
    driver.findElement(By.id("pick3lategameprefix")).sendKeys("LCH");
    driver.findElement(By.id("pick3actearlygmhrs")).click();
    driver.findElement(By.linkText("20")).click();
    driver.findElement(By.linkText("20")).click();
    driver.findElement(By.id("pick3cutoffearlygmhrs")).click();
    driver.findElement(By.linkText("19")).click();
    driver.findElement(By.linkText("50")).click();
    driver.findElement(By.linkText("19")).click();
    driver.findElement(By.linkText("50")).click();
    driver.findElement(By.id("pick3chkearly_3ball")).click();
    driver.findElement(By.id("pick3chkearly_4ball")).click();
    driver.findElement(By.id("pick3chkearly_5ball")).click();
    driver.findElement(By.id("pick3chkearly_borlette")).click();
    driver.findElement(By.id("pick3chkearly_mariage")).click();
    driver.findElement(By.id("pick3inpearly_3ball")).clear();
    driver.findElement(By.id("pick3inpearly_3ball")).sendKeys("100");
    driver.findElement(By.id("pick3inpearly_4ball")).clear();
    driver.findElement(By.id("pick3inpearly_4ball")).sendKeys("1000");
    driver.findElement(By.id("pick3inpearly_5ball")).clear();
    driver.findElement(By.id("pick3inpearly_5ball")).sendKeys("10000");
    driver.findElement(By.id("pick3inpearly_borlette1")).clear();
    driver.findElement(By.id("pick3inpearly_borlette1")).sendKeys("50");
    driver.findElement(By.id("pick3inpearly_borlette2")).clear();
    driver.findElement(By.id("pick3inpearly_borlette2")).sendKeys("20");
    driver.findElement(By.id("pick3inpearly_borlette3")).clear();
    driver.findElement(By.id("pick3inpearly_borlette3")).sendKeys("10");
    driver.findElement(By.id("pick3inpearly_mariage")).clear();
    driver.findElement(By.id("pick3inpearly_mariage")).sendKeys("1000");
    driver.findElement(By.id("pick3Copy-Late-Game")).click();
    driver.findElement(By.id("btn_pick3_save")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
    driver.findElement(By.id("btnpick3")).click();
  }

  @AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

