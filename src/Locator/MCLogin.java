package Locator;


import org.testng.annotations.*;
import static org.testng.Assert.*;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class MCLogin extends BaseClass {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeClass(alwaysRun = true)
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "http://192.168.1.173/";
    //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testMCLogin() throws Exception {
	
	  // Connect with MC User  
	  connectMC();

	  // Create First Admin-User
	  createFirstuser();
	 
	  // Create head location    
	  createHeadLocation();
    
	  // Create a location
	  createLocation();
     
	  
/**	  
//  WebElement dropdown = driver.findElement(By.xpath("/html/body/div[11]/div[2]/form/div[1]/table/tbody/tr[2]/td[2]/button"));
//   Select headlocation1= new Select(driver.findElement(By.xpath("//button[@type='button']")));
//   Select headlocation1= new Select(driver.findElement(By.xpath("/html/body/div[11]/div[2]/form/div[1]/table/tbody/tr[2]/td[2]/button")));
//   headlocation1.selectByVisibleText("bangalore");
    
//   WebElement select = driver.findElement(By.xpath("/html/body/div[11]/div[2]/form/div[1]/table/tbody/tr[2]/td[2]/button"));
//   List<WebElement> options = select.findElements(By.tagName("button"));
//   for (WebElement option : options) 
//   {
//       if("bangalore".equals(option.getText()))
//           option.click();   
//   }
  
  	
	
    
   // Select dropdown1 = new Select(driver.findElement(By.xpath("(//ul[@class='\"ui-multiselect-checkboxes ui-helper-reset']//*[@id='ui-multiselect-inp_head_location-option-0'])")));
   // dropdown1.selectByVisibleText("bangalore");
  
//    driver.findElement(By.id("txtlocationname")).clear();
//    driver.findElement(By.id("txtlocationname")).sendKeys("BTM Layout");
//    driver.findElement(By.id("txtcreditlimit")).clear();
//    driver.findElement(By.id("txtcreditlimit")).sendKeys("12000");
//    driver.findElement(By.id("setting_password")).clear();
//    driver.findElement(By.id("setting_password")).sendKeys("1234");
//    driver.findElement(By.id("butt-Receipt-Info")).click();
//    driver.findElement(By.id("butt-Add-Location")).click();
//    driver.findElement(By.id("btn_location_save")).click();
//    driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();

	  
//	  //user creation
//    driver.findElement(By.linkText("User")).click();
//    driver.findElement(By.id("btn_add_adminmanager")).click();
//    driver.findElement(By.id("uname")).clear();
//    driver.findElement(By.id("uname")).sendKeys("admin");
//    driver.findElement(By.id("upassword")).clear();
//    driver.findElement(By.id("upassword")).sendKeys("Aqui#123");
//    driver.findElement(By.xpath("(//button[@type='button'])[5]")).click();
//    driver.findElement(By.id("ui-multiselect-ddl_location-option-0")).click();
//    driver.findElement(By.xpath("(//button[@type='button'])[6]")).click();
//    driver.findElement(By.xpath("//div[14]/ul/li/label")).click();
//    driver.findElement(By.id("ui-multiselect-ddl_profile-option-0")).click();
//    driver.findElement(By.id("firstname")).clear();
//    driver.findElement(By.id("firstname")).sendKeys("LP");
//    driver.findElement(By.id("lastname")).clear();
//    driver.findElement(By.id("lastname")).sendKeys("Admin");
//    driver.findElement(By.id("btn_save_adminmanager")).click();
//    driver.findElement(By.xpath("(//button[@type='button'])[3]")).click();
//    driver.findElement(By.cssSelector("a.arr1.logoutArrow")).click();
//   // driver.findElement(By.linkText("Sign Out")).click();
//    driver.findElement(By.id("password")).clear();
//    driver.findElement(By.id("password")).sendKeys("1");
//    driver.findElement(By.id("username")).clear();
//    driver.findElement(By.id("username")).sendKeys("admin");
//    driver.findElement(By.id("input_submit")).click();
//    driver.findElement(By.id("password")).clear();
//    driver.findElement(By.id("password")).sendKeys("Aqui#123");
//    driver.findElement(By.cssSelector("span.txt_Games")).click();
//    driver.findElement(By.id("btnpick3")).click();
//    driver.findElement(By.id("btnpick3gameadd")).click();
//    driver.findElement(By.id("pick3rdboth")).click();
//    driver.findElement(By.id("pick3earlyflimg234ball")).clear();
//    driver.findElement(By.id("pick3earlyflimg234ball")).sendKeys("/home/rahul/Desktop/images/borletteEarly.jpg");
//    driver.findElement(By.id("pick3lateflimg234ball")).clear();
//    driver.findElement(By.id("pick3lateflimg234ball")).sendKeys("/home/rahul/Desktop/images/Albarka.png");
//    driver.findElement(By.id("pick3gamename234ball")).click();
//    driver.findElement(By.id("pick3gamename234ball")).click();
//    driver.findElement(By.id("pick3gamename234ball")).clear();
//    driver.findElement(By.id("pick3gamename234ball")).sendKeys("Chicago");
//    driver.findElement(By.id("pick3gamename234ball")).click();
//    driver.findElement(By.id("pick3earlygamename")).click();
//    driver.findElement(By.id("pick3earlygamename")).clear();
//    driver.findElement(By.id("pick3earlygamename")).sendKeys("Early cago");
//    driver.findElement(By.id("pick3gamename234ball")).click();
//    driver.findElement(By.id("pick3earlygamename")).click();
//    driver.findElement(By.id("pick3earlygamename")).clear();
//    driver.findElement(By.id("pick3earlygamename")).sendKeys("Early Chicago");
//    driver.findElement(By.id("pick3earlygameprefix")).click();
//    driver.findElement(By.id("pick3earlygameprefix")).click();
//    driver.findElement(By.id("pick3earlygameprefix")).clear();
//    driver.findElement(By.id("pick3earlygameprefix")).sendKeys("ECH");
//    driver.findElement(By.id("pick3lategamename")).click();
//    driver.findElement(By.id("pick3lategamename")).clear();
//    driver.findElement(By.id("pick3lategamename")).sendKeys("Late Chicago");
//    driver.findElement(By.id("pick3lategameprefix")).click();
//    driver.findElement(By.id("pick3lategameprefix")).clear();
//    driver.findElement(By.id("pick3lategameprefix")).sendKeys("LCH");
//    driver.findElement(By.id("pick3actearlygmhrs")).click();
//    driver.findElement(By.linkText("20")).click();
//    driver.findElement(By.linkText("20")).click();
//    driver.findElement(By.id("pick3cutoffearlygmhrs")).click();
//    driver.findElement(By.linkText("19")).click();
//    driver.findElement(By.linkText("50")).click();
//    driver.findElement(By.linkText("19")).click();
//    driver.findElement(By.linkText("50")).click();
//    driver.findElement(By.id("pick3chkearly_3ball")).click();
//    driver.findElement(By.id("pick3chkearly_4ball")).click();
//    driver.findElement(By.id("pick3chkearly_5ball")).click();
//    driver.findElement(By.id("pick3chkearly_borlette")).click();
//    driver.findElement(By.id("pick3chkearly_mariage")).click();
//    driver.findElement(By.id("pick3inpearly_3ball")).clear();
//    driver.findElement(By.id("pick3inpearly_3ball")).sendKeys("100");
//    driver.findElement(By.id("pick3inpearly_4ball")).clear();
//    driver.findElement(By.id("pick3inpearly_4ball")).sendKeys("1000");
//    driver.findElement(By.id("pick3inpearly_5ball")).clear();
//    driver.findElement(By.id("pick3inpearly_5ball")).sendKeys("10000");
//    driver.findElement(By.id("pick3inpearly_borlette1")).clear();
//    driver.findElement(By.id("pick3inpearly_borlette1")).sendKeys("50");
//    driver.findElement(By.id("pick3inpearly_borlette2")).clear();
//    driver.findElement(By.id("pick3inpearly_borlette2")).sendKeys("20");
//    driver.findElement(By.id("pick3inpearly_borlette3")).clear();
//    driver.findElement(By.id("pick3inpearly_borlette3")).sendKeys("10");
//    driver.findElement(By.id("pick3inpearly_mariage")).clear();
//    driver.findElement(By.id("pick3inpearly_mariage")).sendKeys("1000");
//    driver.findElement(By.id("pick3Copy-Late-Game")).click();
//    driver.findElement(By.id("btn_pick3_save")).click();
//    driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
//    driver.findElement(By.id("btnpick3")).click();
	  
*
*/
	  
  }

  private void createLocation() {
	// TODO Auto-generated method stub
	    driver.findElement(By.xpath("(//a[contains(text(),'Location')])[3]")).click();
	    driver.findElement(By.xpath("(//ul[@id='bl1']//*[@href='location'])")).click();
	    driver.findElement(By.id("Location-butt")).click();
	    driver.findElement(By.xpath("(//button[@type='button'])[4]")).click();
	    driver.findElement(By.xpath("//button[@type='button']")).click();
	    driver.findElement(By.id("ui-multiselect-inp_head_location-option-0")).click();
	  
	    driver.findElement(By.id("txtlocationname")).clear();
	    driver.findElement(By.id("txtlocationname")).sendKeys("BTM Layout");
	    driver.findElement(By.id("txtcreditlimit")).clear();
	    driver.findElement(By.id("txtcreditlimit")).sendKeys("12000");
	    driver.findElement(By.id("setting_password")).clear();
	    driver.findElement(By.id("setting_password")).sendKeys("1234");
	    driver.findElement(By.id("butt-Receipt-Info")).click();
	    driver.findElement(By.id("butt-Add-Location")).click();
	    driver.findElement(By.id("btn_location_save")).click();
	    driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
	
}

private void createHeadLocation() {
	// TODO Auto-generated method stub
	 // driver.findElement(By.xpath("(//a[contains(text(),'Head Location')])[2]")).click();
	
		String headLocationName = "Bangalore-1";
	    driver.findElement(By.xpath("(//ul[@id='bl1']//*[@href='headlocation'])")).click();
	    driver.findElement(By.id("Location-butt")).click();
	    
//	    String gname = driver.findElement(By.xpath("(//tbody[@id='tfoot_locdata']//tr[last()]//td[@id='td_headloc_name'])")).getText();
//	    WebElement select = driver.findElement(By.xpath("(//tbody[@id='tfoot_locdata']//*[@id='td_headloc_name'])")).iterator();
	    
	    // Head-Location base table 
	    WebElement baseTable  = driver.findElement(By.xpath("(//tbody[@id='tfoot_locdata'])"));

	    ////tbody[@id='tfoot_locdata']//*[@id='tr_locdata']
	    ////tbody[@id='tfoot_locdata']//*[@id='tr_locdata']//td[@id='td_headloc_name']
	    ////tbody[@id='tfoot_locdata']//tr[@id='tr_locdata']//td[2]
	    
	    // To find 2nd row of table	
		WebElement tableRow = baseTable.findElement(By.xpath("//tr[@id='tr_locdata']//td[2]"));

        String rowtext = tableRow.getText();
		System.out.println("Second row of table : " + rowtext);
		 
//	    List<WebElement> options = select.findElements(By.id("tr_locdata"));
//	    List<WebElement> options = select.findElements(By.xpath("(//tr[@id='td_headloc_name'])"));
//
//	    System.out.println("LIST-Element" + options );	    
//	    Iterator<WebElement> itr = options.iterator();
//	    while(itr.hasNext()) {
//	        System.out.println("ITR Value: " + itr.next());
//	    }
//	   for (WebElement option : options) 
//	   {
//		    System.out.println("All Head Loc name ::" + option.getAttribute("td_headloc_name"));
//	   }
	    
//	    System.out.println("All Head Loc name ::" + gname);
//	    
//	    if(headLocationName.equals(gname) )
//	    {
//	    	String newHeadLoctionName = headLocationName + "1";
//		    System.out.println("New Head Loc name ::" + newHeadLoctionName);
//		    driver.findElement(By.id("txtlocationname")).clear();
//		    driver.findElement(By.id("txtlocationname")).sendKeys(newHeadLoctionName);	    	
//	    }else {
//		    driver.findElement(By.id("txtlocationname")).clear();
//		    driver.findElement(By.id("txtlocationname")).sendKeys(headLocationName);	    	
//	    }
//
//
//	    driver.findElement(By.id("ofc_phone_1")).clear();
//	    driver.findElement(By.id("ofc_phone_1")).sendKeys("1234578900");
//	    driver.findElement(By.id("btn_location_save")).click();
//	    driver.findElement(By.xpath("(//button[@type='button'])[2]")).click();
}

private void connectMC() {
	// TODO Auto-generated method stub
	    driver.get(baseUrl + "/managementconsole/welcome");
}

private void createFirstuser() {
	// TODO Auto-generated method stub
    driver.findElement(By.id("username")).clear();
    driver.findElement(By.id("username")).sendKeys("locusplay");
    
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password")).sendKeys("locusplay");
    driver.findElement(By.id("input_submit")).click();
}

@AfterClass(alwaysRun = true)
  public void tearDown() throws Exception {
    //driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
