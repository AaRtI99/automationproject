import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;


public class ScreenShotManagementconsole extends BaseClass
{

	public static void main(String[] args) throws IOException
	{
		driver.get("http://192.168.1.196/managementconsole/welcome");
		
		TakesScreenshot ts= (TakesScreenshot) driver;
		
		File srcFile=ts.getScreenshotAs(OutputType.FILE);
		
		File destFile= new File("./Screenshot/MC.png");
		
		FileUtils.copyFile(srcFile, destFile);
		
		driver.close();
		
		
		
	}

}
