import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Navigation {

	public static void main(String[] args) throws InterruptedException 
	{
		 System.setProperty("webdriver.gecko.driver", ".\\driver\\geckodriver.exe");

		WebDriver driver=new FirefoxDriver();
		driver.get("https://www.google.co.in");
		
		driver.navigate().to("https://www.facebook.com/");
		 
		driver.navigate().back();
		
		Thread.sleep(2000);
		driver.navigate().forward();
		
		Thread.sleep(2000);
		
        driver.navigate().refresh();
		
		Thread.sleep(2000);
		
		driver.quit();

	}

}
